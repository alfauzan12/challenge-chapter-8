import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import NavBar from "./Navbar";
import HomePage from "./routes/CreatePlayer";
import SearchPage from "./routes/SearchPlayer";
import EditPlayerPage from "./routes/EditPlayer";

function App() {
  return (
    <Router>
      <div>
        <NavBar />
          <Route path="/editplayer">
            <EditPlayerPage />
          </Route>
          <Route path="/player">
            <SearchPage />
          </Route>
          <Route path="/">
            <HomePage />
          </Route>
      </div>
    </Router>
  );
}

export default App;